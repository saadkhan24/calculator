#ifndef CALCULATOR_H
#define CALCULATOR_H

template <class T>
class calculator
{
 public:
   calculator(T input);
   T add(T input);
   T subtract(T input);
   T multiply(T input);
   T divide(T input)
   T square();

 private:
   T _result;
};

template class calculator<int>;
template class calculator<float>;
template class calculator<double>;

#endif