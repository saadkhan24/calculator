#include "calculator.h"

template <class T>
calculator<T>::calculator(T input)
   : _result(input)
{}

template <class T>
T calculator<T>::add(T input)
{
   _result = _result + input;
   return _result;
}

template <class T>
T calculator<T>::subtract(T input)
{
   _result = _result - input;
   return _result;
}

template <class T>
T calculator<T>::multiply(T input)
{
   _result = _result * input;
   return _result;
}

template <class T>
T calculator<T>::divide(T input)
{
   _result = _result / input;
   return _result;
}

template <class T>
T calculator<T>::square()
{
   _result = _result * _result;
   return _result;
}