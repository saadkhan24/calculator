# C++ Calculator
This is a simple C++ calculator.
Build the repo using the following commands:
```
mkdir build
cmake ..
make
```

In order to build unit tests, use the following commands:
```
mkdir build
cmake -DBUILD_TESTS=True ..
make
```

Once compiled, the main application executable will be generated within `executable` directory in build directory and unit tests executable will be generated within `tests` directory in the build directory.