#include "calculator.h"
#include "catch.hpp"

TEST_CASE("multiply_int", "[multiply]")
{
   int        input_number = 2;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.multiply(5) == 10);
   REQUIRE(my_calculator.multiply(3) == 30);
   REQUIRE(my_calculator.multiply(50) == 1500);
}

TEST_CASE("multiply_float", "[multiply]")
{
   float      input_number = 5.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.multiply(10) == 50.0);
   REQUIRE(my_calculator.multiply(4) == 200.0);
   REQUIRE(my_calculator.multiply(11) == 2200.0);
}

TEST_CASE("multiply_double", "[multiply]")
{
   double     input_number = 10.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.multiply(15) == 150.0);
   REQUIRE(my_calculator.multiply(5) == 750.0);
   REQUIRE(my_calculator.multiply(20) == 15000.0);
}