#include "calculator.h"
#include "catch.hpp"

TEST_CASE("divide_int", "[divide]")
{
   int        input_number = 100;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.divide(5) == 20);
   REQUIRE(my_calculator.divide(4) == 5);
   REQUIRE(my_calculator.divide(2) == 2);
}

TEST_CASE("divide_float", "[divide]")
{
   float      input_number = 100.0;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.divide(5) == 20.0);
   REQUIRE(my_calculator.divide(4) == 5.0);
   REQUIRE(my_calculator.divide(2) == 2.5);
}

TEST_CASE("divide_double", "[divide]")
{
   double     input_number = 100;
   calculator my_calculator(input_number);
   REQUIRE(my_calculator.divide(5) == 20.0);
   REQUIRE(my_calculator.divide(4) == 5.0);
   REQUIRE(my_calculator.divide(2) == 2.5);
}